<?php
/**
 *
 * Page template for all 404 (not found) pages of the <CLIENT-NAME> <YEAR> website theme
 * Outputs a user-friendly "page not found" message to inform users the page does not exist
 * Finishes after the output of the error message
 *
 * @package NAMEOFTHEME
 */

get_header();
?>

	<article class="page">
		<section class="article-main">
			<header class="article-main__header">
				<h1>Sorry!</h1>
			</header>
			<p>That page does not exist. Please <a href="<?php print esc_url( home_url() ); ?>" title="Return to the Home page">go back</a> and try another page.</p>
		</section>
	</article>

<?php
get_footer();
