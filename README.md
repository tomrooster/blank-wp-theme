# <CLIENT-NAME> <YEAR> theme

## Contents

- [Initial Setup](#initial-setup)
  - Mac
  - Windows
- [Theme Setup](#theme-setup)
- [Files & Folders](#files-folders)
  - package.json
  - backstop settings
  - /src/ & /dist/
  - /reports/
  - /acf-json/
  - Git
- [Gulp commands](#gulp-commands)
  - Init
  - Watch
  - Build
  - Individual tasks
- [Sass](#sass)
  - Import maps
  - Variables, Mixins & Extenders
  - Breakpoints
  - Output
  - Comments
  - CSS output / loading
  - Autoprefixer
  - Normalize
  - Linting
- [Javascript](#javascript)
  - Minification
  - Linting
  - Output / loading
- [Images](#images)
- [PHP](#php)
  - header.php / header-head.php
  - footer.php / footer-scripts.php
  - functions
  - searchform.php
- [Styleguide](#styleguide)
  - content
  - config
  - CSS
- [Service workers](#serviceworkers)
  - Files
  - Other functions
  - Notes
- [Backstop](#backstop)
  - Setup
  - Testing
- [No Gulp?](#no-gulp)

<a name="initial-setup"></a>
## Initial setup

Skip to [Theme Setup](#theme-setup) if node/gulp work on your computer already.

Gulp requires Node.js and npm, so if they're not installed to that. It's available at [nodejs.org/en/](https://nodejs.org/en/) the npm instructions are at [www.npmjs.com/get-npm](https://www.npmjs.com/get-npm)

(**Note:** you may get permission issues/need to use `sudo` to install npm packages globally, this can be fixed by changing the permissions with this snippet `sudo chown -R $(id -u):$(id -g) /usr/local/lib/node_modules/`)

Global installations are done with `-g`, most other installations are done per-project, the only 4 likely to need installing globally are

- gulp - `npm install gulp-cli -g`
- backstop - `npm install -g backstopjs`
- composer - see [https://getcomposer.org/doc/00-intro.md](https://getcomposer.org/doc/00-intro.md)

### Mac

It's probably best to start by installing the latest version of XCode & the XCode command line tools (and launch XCode as it requires an agreement before it will work).

### Windows
- This guide is helpful [ruleoftech.com/2015/setting-up-bower-and-gulp-in-windows](http://ruleoftech.com/2015/setting-up-bower-and-gulp-in-windows)
- If there's a problem with npm not being in the PATH see [stackoverflow.com/a/46320908](https://stackoverflow.com/a/46320908)
- It will need python is it's not already installed. (and may have the same PATH issues see [stackoverflow.com/a/32408138](https://stackoverflow.com/a/32408138))
- and it needs the Windows build tools (run as Admin) `npm install --global --production windows-build-tools`
- Also if there are problems with node sass try `npm rebuild node-sass`

And see "[No Gulp?](#no-gulp)" if there are problems with these instructions. 

<a name="theme-setup"></a>
## Theme Setup

Once that's done you should be ready to go with the gulp based theme.

1. `cd /path/to/the/theme/folder`
2. run `gulp --version` to make sure v4 is installed
3. `npm install` this will install all the required modules in the package.json or package-lock.json
4. `composer create-project wp-coding-standards/wpcs --no-dev && composer install` does the WordPress coding standards & installs the composer bit
5. Do a find and replace for &#x27;&lt;CLIENT-NAME&gt;&#x27;, &#x27;&lt;YEAR&gt;&#x27;, &#x27;&lt;CLIENT-URL&gt;&#x27;, 'NAMEOFTHEME' (the directory name), with the appropriate info.
6. `gulp init` this will do the initiation for gulp (which moves any bower/npm files and does a build)

<a name="files-folders"></a>
## Files & Folders

A quick overview of what comes in the theme

### `package.json` & `package-lock.json`

These files tell npm what's getting installed. package.json in the readable list of things added by the developer (via `npm install $packageName --save-dev`). The package-lock.json is the generated file that determines exactly which versions get installed where (as different modules may rely on different versions of the same sub-module).

### `composer.json` & `composer.lock`

This is the config for composer, it only installs `phpcodesniffer-composer-installer` which is needed by the WordPress Coding Standards to work correctly.

### `config/backstop-settings.js` & `config/paths.js`

This is the config for backstop & is covered below, `config/paths.js` is an optional config.

### `/src/` & `/dist/`

The majority of the front end files start in `/src/` and are processed into `/dist/`. The folders in here are all self-explanatory: images, scss, js

### `/reports/` & `/backstop_data/`

This is for reports from Backstop so initially empty. They shouldn't be in git normally but there could be an argument for having backstop reference files in git depending on the project

### `/acf-json/`

This is included, with a blank index.php so any ACF work will be saved into a .json file that can go into git. Primarily as a bit of a backup rather than just having it in a local DB during development.

### Git

Some folders should be excluded from git

- `/bower_components/`
- `/node_modules/`
- `/reports/`
- `/backstop_data/`
- `/styleguide/` (unless needed on the server, in that case it might be best to leave it out of git until dev work is mostly done)

- `/dist/` can be added to that list if the deployment is going to run gulp and do the compile on git-push.
- `/src/sass/import-maps/` should be committed, nothing is committed in there by default but should be after the `gulp init` on a new project

**Project wide search**: If you use project-wide search you'll want to disable searching most or all of the files excluded from git as there's normally thousands in /node_modules/ and it can slow down searching & give lots of pointless results

<a name="gulp-commands"></a>
## Gulp commands

There's a few gulp commands, the main ones are watch/default & build which run a selection of other commands.

### Init

Init does `gulp movefiles`, which puts a few files into `/src/`, and does an intial `gulp build`, so if it's an exisiting site on a new machine it should be good to go, or if it's a new site there'll be a basic file with Normalize.css and some other defaults.

### Watch

Watch is the default command, so is run with `gulp`, this does the following

- compiles sass on file save, also updates the styleguide
- javascript will be put into dist on save without compression
- new image saves will be copied & compressed into dist with the same filename

### Build

Build is the command for getting code ready to deploy, run with `gulp build`

- compiles the sass in the compressed format
- compresses/uglifies the javascript and removes console logs
- copies & compresses any images into dist
- builds the styleguide

### Individual tasks

All tasks can be run individually as well, so if you just want to test the speed this can be done with `gulp speed`

<a name="sass"></a>
## Sass

The majority of sass updates will be covered with the gulp watch or gulp build (which just changes the output). This functions the same as most other ways of compiling sass, the main difference is…

### Import maps

The import maps are created by gulp on build/watch. The function goes through each of the top level directories and creates a list of `@import`s, then for each one adds a partial into `/src/sass/import-maps/`. This makes modular re-usable sass much easier, and git commits clearer. It's a different way of coding with lots of `.scss` files compiling into a single `.css` file.

There are some gotchas to this

- the ordering matters, a variable added later in the tree can't be used by an earlier file (hence `/mixins_1/` and `/mixins_2/`, where the files in 2 can depend on files in 1)
- it's a slightly higher learning curve
- there normally needs to be some kind of systematic approach to managing files (such as Atomic design or SMACSS) to make the extra effort worth while

There are 2 main sass files

1. `styles.scss` - which contains the vital above-the-fold styles (anything in `/sass/reset/` and `/sass/vital/`)
2. `other-styles.scss` - which contains everything else (in `/sass/elements/`) 

Both files start in the same way

```scss
// Import vendor libraries.
@import "imports";

// Import various partials
@import "import-maps/variables";
@import "import-maps/mixins";
```

This allows both files to use any bower/npm imports, and access the same variables, mixins and extenders

styles.scss then has:

```scss
@import "import-maps/reset";
@import "import-maps/vital";
```

and other-styles.scss:

```scss
@import "import-maps/elements";
```

There's also 

3. `style-guide.scss` - which does the style guide specific css, [see the Stylguide section](#styleguide)
4. `editor-style.scss` - which does the WYSIWYG editor styles for WordPress, with access to the theme's variables & mixins.

### Variables, Mixins & Extenders

**Variables & Extenders** some defaults are included & documented in the styleguide. Extenders go in variables as they're not really useful enough for a separate folder.

**Mixins** Any mixins should go in either `/mixins_1/` if it's not dependent on any other mixins, or `/mixins_2/` if it uses one of the other mixins (such as having a print style set).

Each file should have one mixin [that gets used elsewhere] and be documented in the styleguide

<a name="breakpoints"></a>
### Breakpoints

Breakpoints are done using Breakpoint Sass which makes mobile-first responsiveness very simple. Starting with some variables for breakpoints like so:

```scss
$bp-vsmall: 25.875em; // 414px 
$bp-small:  32.5em;   // 520px
$bp-medium: 48em;     // 768px
$bp-large:  60em;     // 960px
$bp-xl:     80em;     // 1280px
$bp-xxl:   100em;     // 1600px
$bp-hd:    120em;     // 1920px
```

The default breakpoints are *em* based as it's more reliable for users who change font size or zoom, the pixels above are on the assumption that it's the normal 16px == 1em. There are also actual pixel equivalents for these (and max-width versions, that are 1px narrower) which can be more useful in some cases.

For example a gallery that goes from a single column on mobile up to 4 columns on desktop:

```scss
.gallery {
  display: flex;
  flex-wrap: wrap;
}
.gallery__image {
  width: 100%;
  @include breakpoint($bp-small) {
    width: 50%;
  }
  @include breakpoint($bp-medium) {
    width: 33.333%;
  }
  @include breakpoint($bp-large) {
    width: 25%;
  }
}
```

In most cases code should be organised as above, going from smallest to largest, there may be cases where something is only applying between 2 breakpoints, in that case the code is:

```scss
.selector {
  @include breakpoint($bp-px-small $bp-mw-medium) {
    // Styles
  }
}
```

Note `$bp-px-` being used for the pixel base min-width, and `$bp-mw-` for the max-width (which is 767px rather than the 768px used for $bp-medium, so the code doesn't overlap when exactly 768px wide).

### Print styles

Print styles are done in a similar way to breakpoints with the print mixin, this doesn't take any arguments as the media query is always the same. See demo in the mixins documentation

### Output

When using `gulp` the output is in the expanded style. When using `gulp build` it's the compressed output style.

### Comments

Comments need to be formatted in a specific way for the styleguide to pick it up & document correctly. See the [spec](https://github.com/kss-node/kss/blob/spec/SPEC.md) for a full guide and `/sass/elements/buttons/_button.scss` for a demo.

### CSS output / loading

The 2 separate sass files mean there are 2 css files to load. `styles.css` is loaded as normal so is kept as small as possible, with the content that appears at the top (main header, article header, navigation), to avoid render blocking. `other-styles.css` (and any other css files) is then loaded using a small Javascript snippet (with a `<noscript>` fallback) so it loads asynchronously:

These settings, as well as optional `<link rel="preload">`s are set up from an array of JS & CSS files in `front-end.php`.

### Autoprefixer

Autoprefixer is a very useful tool that adds the `-moz / -webkit etc…` prefixes as needed, it takes an argument of how far back it should check (either last *X* versions, or from version *X*), and includes the prefixes needed. This keeps the sass clean but the output functional.

The default is IE10+ and latest 2 versions of decent browsers. This should be changed per-project as needed, especially if old IE code can be removed.

### Normalize

I've included `normalize.css` as the css reset, it's not a pure reset, but sets some decent defaults across browsers that are easily overridden. It's done with the scss version which imports a sass partial straight into the main file in `_base.scss`.

### Linting

The sass is [linted](https://en.m.wikipedia.org/wiki/Lint_(software)) on watch & build. So any inconsistencies will get warnings in the terminal output, for example the config is set to tabs by default so using spaces for indentation will show a warning. The settings can be changes in `config/sass-lint.yml`, see the [rules documents](https://github.com/sasstools/sass-lint/tree/master/docs/rules) for full descriptions of all the options.

Some files have specific options disabled, mostly in the kss and editor styles where a lot of the code is copy & pasted. But also file specific changes like `!important` being allowed in some places.

All the notes are set to show a warning in the terminal, **nothing will stop working or block code from being committed**.

<a name="javascript"></a>
## Javascript

A default `scripts.js` is included with some basic common functions. It can mostly be used as-is without requiring jQuery (there are a couple of useful Owl Carousel functions in there that use jQuery, but it'll be used in that case anyway).

Managing files will vary per site. The general rule will be to combine & minify the js to get the best performance. But if a large script is only needed on a specific page it can be better to split it out. jQuery is included by default, but can easily be removed if not used/needed, it's enqueued in the functions as Contact Form 7 requires something enqueued with the name 'jquery', if a concatinated set of 3rd parts scripts is used this can replace the jQuery file.

### Minification

When building the javascript is minified & has console.log files removed, which makes debugging easier (as the console.logs can be left in place but not affect the live sites).

### Linting

The javascript is [linted](https://en.m.wikipedia.org/wiki/Lint_(software)) on watch & build. So any inconsistencies will get warnings in the terminal output, for example the config is set to tabs by default so using spaces for indentation will show a warning. The settings can be changed in `config/eslint-config.json`, which is based on the standard eslint config, see the [rules documents](https://eslint.org/docs/rules/) for full descriptions of all the options.

Out of the box `scripts.js` does have some `no-unused-vars` warnings. These are likely to be used in actual projects, but the warning is useful when actually in development so has been left on.

All the notes are set to show a warning in the terminal, **nothing will stop working or block code from being committed**.

### Output / loading

The minified js is output in `/dist/`, These files are loaded, and optional `<link rel="preload">`s are added, from an array of JS & CSS files in `front-end.php`.

<a name="images"></a>
## Images

Not much happens with images, they're just copied & compressed into the `/dist/images/`. [Imagemin](https://www.npmjs.com/package/gulp-imagemin) just does the following by default, and can run additional compression libraries.

- gifsicle — Compress GIF images
- jpegtran — Compress JPEG images
- optipng — Compress PNG images
- svgo — Compress SVG images

<a name="php"></a>
## PHP

A quick overview of the .php files in the theme, they're generally pretty basic and empty.

### `header.php` / `header-head.php`

The normal header.php file is split into 2 files:

- `header.php` just concentrates on the html output, so the visible stuff at the top of each page. It also does the import of header-head.php
  - There's the html for the old IE message
  - A skip link for accessibility, with some default styles
  - The default logo, which will need replacing and styling. The styleguide will get the same logo by default
  - A main-menu is there on the assumption it'll be added
- `header-head.php` includes everything that goes inside the `<head>` tag. There are a few things in here by default that need changing
  - The DNS preconnect / prefetch `<link>` tags, add one for each 3rd-party domain that's used (including the static ones that are used to serve css/fonts/images). This will speed up loading the page as the initial connection is done in the background before the page starts to load the css/js/images/fonts
  - Add the stylesheets, see "CSS output / loading" for details on how this works, the defaults `styles.css` and `other-styles.css` are already there
  - A favicon and manifest are there, follow the todo instructions to update these
  - The css for the old IE (less than v10) is there, as minified as possible it probably won't need to change

### `footer.php` / `footer-scripts.php`

As with the head the footer.php is split into 2 files:

- `footer.php` concentrates on the html, it's blank by default with an empty `<footer>` & imports footer-scripts.php
- `footer-scripts.php` is for including scripts either manually of via `wp_footer`, by default there's:
  - jquery (3.3.1 minified via `wp_footer`)
  - script.js 

### functions

`functions.php` loads the files from the functions directories, these do the usual tidy up things, as well as some additional bits for this theme:

- Add the styleguide to the WP Admin menu
- Add the editor styles file to TinyMCE
- Removes Gutenburg, jQuery, and WP Embed
- Functions use the `Rooster\NAMEOFTHEME` namespace to ensure they won't clash with other code.

### `searchform.php`

Is based on the form from the TwentySeventeen theme

### Linting

PHP files are linted on watch & build. So any inconsistencies will get warnings in the terminal output, for example the config is set to tabs by default so using spaces for indentation will show a warning. The settings can be changed in the `php:lint` task in the gulpfile and are based on the WPCS defaults with a few documented changes.

The PHP linting is a bit slow so it might be better to disable the watch command and either run it manually or just on build.

All the notes are set to show a warning in the terminal, **nothing will stop working or block code from being committed**.


<a name="styleguide"></a>
## Styleguide

The styleguide is the [node](https://github.com/kss-node/kss-node) version of [KSS](https://github.com/kneath/kss). Using the basic setup from [CSS tricks](https://css-tricks.com/build-style-guide-straight-sass/) and [some Gulp specific changes](https://blog.greggant.com/posts/2018/01/16/integrating-node-kss-with-gulp.html) the guide will update every time you build or save a sass file while running `gulp`.

### Content

You can view the styleguide at [/wp-content/themes/NAMEOFTHEME/styleguide/](/wp-content/themes/NAMEOFTHEME/styleguide/) and a link is added in /wp-admin/

The homepage is the content from the README.md file, so can be easily edited and kept in git. The rest of the sections should be set out in roughly the same structure as the sass folders and partials, so top level headings of elements, mixins, variables, vital.

Maintaining the html can be a problem, especially when the styling is laying out other components. If a site becomes too complex it will be worth looking at using handlebars or twig to manage it, but keeping the styleguide focused on smaller components should be good enough in most cases. The other thing to note is media queries, as there's a sidebar on the KSS page they might have unexpected outcomes, there are "fullscreen" and "open in new window" buttons to cover this.

### Config

The config is at the top of the gulpfile. If any stylesheets or js are added it will need to be updated there

### CSS

There is a directory `/style-guide/` for the stylguide's specific css, with 3 files

- `_demo-styles.scss` - This just does minor display changes & enables demoing things that don't get classes by default (such as the font stacks). Any css for demos should use `sg--` at the start of a class, and these styles should never be used in production.
- `_readme.scss` - for styles specific to this readme page
- `_kss.scss` - a complete replica of the KSS styles with some extra specificity, to simply do some overrides

<a name="serviceworkers"></a>
## Service workers

[Service workers](https://developers.google.com/web/fundamentals/primers/service-workers/) have some basic, mothballed, setup in the gulp file.

- `gulp serviceworker:init` will create a .json file with the current version number
- `gulp serviceworker:build` creates a service worker file (`sw.js`) in the webroot, and increases the version number
  - `sw.js` does a basic bit of caching. A new cache is set up based on the version number, the old cache is deleted, and the `fetch` listener is added to load files from the cache rather than the network. 
- by default these commands don't run, when it comes to creating a service worker running `gulp serviceworker:init` and adding `serviceworker:build` to `gulp build` will ensure it gets updated each time something new is deployed

If the service worker is being used this code will need to be added in a `<script>` tag in `footer-scripts.php`, this sets the service worker to load after everything else so it won't have a negative effect for the user.

```javascript
if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {
    navigator.serviceWorker.register('/sw.js');
  });
}
```

### Files

The following files are in the default array, this will probably need to change with each project.

- dist/css/styles.css
- dist/css/other-styles.css
- dist/js/jquery.min.js
- dist/js/scripts.js

The use of a service worker has the big advantage that each page will only need the .js files relevant to it. E.g. if there's a carousel and parallax effect on the homepage, but no-where else, it can be loaded for just that template. But it can also be added to the service worker, so if someone lands on another page the .js for the homepage will be cached in the background before they visit it.

A similar thing could also be done for the css, if the homepage is completely different to the rest of the site it could get a specific `homepage.css` file, with the regular css being added to the service worker, so the users get the homepage loading ASAP, but get the css for the rest of the site loading in the background.

Images & locally hosted fonts can also be added to the cache. Especially useful if the theme is using a sprite that contains most/all of the images.

### Other functions

Service workers can do much more than cache, such as offline modes and notifications. If these are needed it will probably require a different method of creating the service worker file. Possible adding the file list / cache version number to a .json file that the service worker reads?

See [is service worker ready?](https://jakearchibald.github.io/isserviceworkerready/resources.html) for resources. 

### Notes / Gotchas

- Service workers require either running from `http://localhost` or on `https` so the dev setup will need to take this into account.
- The `sw.js` file needs to be in the webroot rather than the theme folder, so will need to work with whatever deployment method is being used
- Fetching 3rd-party urls can be a problem
- Something like [Service Worker Precache](https://github.com/GoogleChromeLabs/sw-precache) might be a better option if there are lots of files to use
- As this is primarily for caching files versions might be an issue, by default this handles it in the simplest / stupidest fashion of replacing the entire cache each time, which might not be ideal

<a name="backstop"></a>
## Backstop

BackstopJS does visual regression testing, so will look at page(s) on 2 sites, take screenshots at various sizes and do a comparison to see what's changed. If a lot has changed it will be highlighted.

The main use for this is to stop regression bugs when css changes after go live it can be easily tested to make sure a change on 1 page doesn't inadvertantly brake anything else.

### Setup

Run `backstop genConfig` to get the initial folders/files in place. These should be excluded from Git.

Set up will depend on how it's being used, once a site's live it's usually best to have an array of pages being tested that cover a wide range of design elements.

Here are a couple of blog posts on setup (one by me) that allows varying config either of some default pages, a specific page

- [BackstopJS Configuration](https://fivemilemedia.co.uk/blog/backstopjs-javascript-configuration)
- [BackstopJS part deux config & makefile](https://www.metaltoad.com/blog/backstopjs-part-deux-javascript-config-and-makefile)

### Testing

Once the setup is done the testing can be done with a couple of commands, a reference and a test. The reference will normally be the live site (but could be the dev site if being compared with local dev), and the test will look at dev or local. The following commands would create a reference for a live contact page, then test it against the dev version.

`backstop reference --configPath=config/backstop-settings.js --refhost=http://live.example.com --paths=/contact`

`backstop test --configPath=config/backstop-settings.js --testhost=http://dev.example.com --paths=/contact`

<a name="no-gulp"></a>
## No Gulp?

If it isn't possible to use gulp for any reason it should be possible to get around it

### Quick fixes

If there just needs to be a quick bit of css or javascript temporarily adding a new `temp.(css|js)` file, with an appropriate note to fix later will be fine.

### Sass

Updating the SASS should be fine with compass as long as the `/src/sass/import-maps/` directory has been committed to git.

This would miss out on a couple of extra features, Autoprefixer would need running. And CSSNano would no longer run, but that would just mean a slight lack of compression on the css.

### Javascript

As the javascript is minified the simplest solution would be doing any updates in `/src/js/scripts.js` and manually copying that file to `/dist/js/`, it loses the minification, but does mean when gulp can be run in future it should just work.

If there are 3rd party scripts they may be combined into a single file, if a new one is needed adding as a seperate file would be the simplest solution.

### Move files

If gulp has never been run the file listed in the movefiles task will need to be manually copied into place.

### Images

Any new/updated images would need to go into both the `src` and `dist` folders. If possible running the images in `dist` through [ImageOptim](https://imageoptim.com/mac) would be good.