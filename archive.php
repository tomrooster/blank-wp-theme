<?php
/**
 *
 * Default Archive template for the <CLIENT-NAME> <YEAR> website theme
 * Outputs a list of posts, newest first, for any of the following pages: category/tag/taxonomy/CPT/author/date
 * This will be used when no other specific template(s) can be found (e.g. category.php)
 * Finishes at the end of 'the loop' - the query that outputs the posts
 *
 * @package NAMEOFTHEME
 */

get_header();
?>

	<article class="page">
		<section class="article-main">
			<header class="article-main__header">
				<h1>Archive</h1>
				<?php // Archive loop here.. ?>
			</header>
		</section>
	</article>

<?php
get_footer();
