<?php
/**
 *
 * Page template for all attachment pages (images/video/etc.) of the <CLIENT-NAME> <YEAR> website theme
 * Outputs either a full-size version of the attachment itself (e.g. jpg), or a downloadable link (e.g. pdf)
 * Finishes at the end of 'the loop' - the query that outputs the attachment
 *
 * @package NAMEOFTHEME
 */

get_header();
?>

	<article class="page">
		<section class="article-main">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					?>
				<header class="article-main__header">
					<h1><?php print esc_html( basename( $post->guid ) ); ?></h1>
				</header>	
					<?php
					if ( wp_attachment_is_image( $post->id ) ) {
						$image = wp_get_attachment_image_src( $post->id, 'full' );
						?>
						<a href="<?php print esc_attr( wp_get_attachment_url( $post->id ) ); ?>" target="_blank" title="Open this image">
							<img src="<?php print esc_attr( $image[0] ); ?>" width="<?php print esc_attr( $image[1] ); ?>" height="<?php print esc_attr( $image[2] ); ?>" alt="<?php print esc_attr( basename( $post->guid ) ); ?>">
						</a>
						<?php
					} else {
						?>
						<p>
							<a href="<?php print esc_attr( wp_get_attachment_url( $post->ID ) ); ?>" target="_blank" title="Download this attachment">Download this attachment</a>
						</p>
						<?php
					}
				endwhile;
			endif;
			?>
		</section>
	</article>
		
<?php
get_footer();
