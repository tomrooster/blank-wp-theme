<?php
/**
 *
 * Category template for the <CLIENT-NAME> <YEAR> website theme
 * Outputs a list of posts, newest first, in a particular category
 * Finishes at the end of 'the loop' - the query that outputs the posts
 *
 * @package NAMEOFTHEME
 */

get_header();
?>

	<article class="page">
		<section class="article-main">
			<header class="article-main__header">
				<h1>Category</h1>
			</header>
			<?php // Category loop here.. ?>
		</section>
	</article>

<?php
get_footer();
