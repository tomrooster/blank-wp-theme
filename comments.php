<?php
/**
 *
 * Template for displaying user comments of the <CLIENT-NAME> <YEAR> website theme
 * Outputs a list of existing comments for the current post, as well as the 'add new comment' form
 * Finishes at the end of 'the loop' - the query that gets the list of comments
 *
 * @package NAMEOFTHEME
 */

?>

<h1>Comments</h1>

<?php
// If this post allows comments, display the list of comments and the comment form.
if ( comments_open() ) {
	// Output the list of comments.
	$no_of_comments = get_comments_number( get_the_ID() );

	if ( $no_of_comments === 0 ) {
		?>
		<h2>No comments yet - be the first!</h2>
		<?php
	} else {
		?>
		<h2><?php print esc_html( $no_of_comments ); ?> Comment<?php print ( $no_of_comments > 1 ) ? 's' : ''; ?></h2>
		
		<?php
		$this_post_id = get_the_ID();

		$args = array(
			'post_id'	=> $this_post_id,
			'post_type' => 'post',
			'status'	=> 'approve',
			'type'		=> 'comment',
		);
		$the_comments = get_comments( $args );
		$count = 1;

		foreach ( $the_comments as $the_comment ) {
			// Comment loop here..
			$count++;
		}
	}

	// Output the comments form here..
} else {
	// Otherwise, display a nice user-friendly error message.
	?>
	<h1>Comments are disabled for this post.</h1>
	<?php
}
?>
