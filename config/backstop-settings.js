/*
	Set up some variables
 */
var arguments = require('minimist')(process.argv.slice(2)); // grabs the process arguments
var defaultPaths = ['/']; // By default it just checks the homepage
var scenarios = []; // The array that'll have the pages to test

/*
	Work out the environments that are being compared
 */
// The host to test
if (!arguments.testhost) {
	arguments.testhost	= "http://test.host"; // Default test host
}
// The host to reference
if (!arguments.refhost) {
	arguments.refhost	= "http://ref.host"; // Default ref host
}


/*
Paths file should be in the format
```
var pathConfig = {};

pathConfig.array = [
  '/',
  '/contact/',
  '/cookies/'
]

module.exports = pathConfig;
```
*/

if (arguments.paths) {
	pathString = arguments.paths;
	var paths = pathString.split(',');
} else if (arguments.pathfile) {
	var pathConfig = require('./'+arguments.pathfile+'.js');
	var paths = pathConfig.array;
} else {
	var paths = defaultPaths; // keep with the default of just the homepage
}

for (var i = 0; i < paths.length; i++) {
	scenarios.push({
		"label": paths[i],
		"referenceUrl": arguments.refhost+paths[i],
		"url": arguments.testhost+paths[i],
		"hideSelectors": [],
		"removeSelectors": [],
		"selectors": [],
		"readyEvent": null,
		"delay": 500,
		"misMatchThreshold" : 0.1
	});
}

module.exports =
{
	"id": "dev_test",
	"viewports": [
		{
			"name": "small",
			"width": 320,
			"height": 480
		},
		{
			"name": "medium",
			"width": 768,
			"height": 1024
		},
		{
			"name": "large",
			"width": 1024,
			"height": 768
		},
		{
			"name": "xlarge",
			"width": 1440,
			"height": 900
		}
	],
	"scenarios":
		scenarios
	,
	"paths": {
		"bitmaps_reference":	"backstop_data/bitmaps_reference",
		"bitmaps_test":			"backstop_data/bitmaps_test",
		"casper_scripts":		"backstop_data/casper_scripts",
		"html_report":			"backstop_data/html_report",
		"ci_report":			"backstop_data/ci_report"
	},
	"casperFlags": [],
	"engine": "puppeteer",
	"report": ["browser"],
	"debug": true
};