<?php
/**
 *
 * Template for displaying the footer scripts of the <CLIENT-NAME> <YEAR> website theme
 * Outputs both hard coded and wp_footer javascript files.
 *
 * @package NAMEOFTHEME
 */

$main_js_time = gmdate( 'YmdHi', filemtime( get_stylesheet_directory() . '/dist/js/scripts.js' ) );

// WordPress enqueued JavaScript.
wp_footer();

// All other JS files.
print '<script src="' . esc_url( get_template_directory_uri() . '/dist/js/scripts.js?v=' . $main_js_time ) . '"></script>';

/*
 * Contact specific scripts
if ( is_page_template('templates/page-contact.php') ) {
	print '<script type="text/javascript" defer src="https://maps.googleapis.com/maps/api/js?key=' . esc_attr( Theme\google_api_key() ) . '"></script>';
}
*/

// Any other scripts should go here, things like analytics, live chat etc...
