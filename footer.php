<?php
/**
 *
 * Template for displaying the footer html of the <CLIENT-NAME> <YEAR> website theme
 * Outputs the html file and loads the footer-scripts.php
 *
 * @package NAMEOFTHEME
 */

?>
			</main><?php // END content wrapper. ?>
			<footer class="main-footer">
			
			</footer>
			<?php // No jQuery cookie prefs, based on https://codepen.io/gschiavon/pen/wvoWZwB. ?>
			<section id="cookies-banner" class="cookies-banner">
				<div><?php the_field( 'cookies_message', 'option' ); ?></div>
				<button id="cookie-consent" class="cookies-banner__button">Accept Cookies</button>
				<?php
				// The privavt policy type page. Set in ACF sitewide.
				$cookies_link = get_field( 'cookies_read_more', 'option' );
				if ( $cookies_link ) {
					print '<a href="' . esc_url( $cookies_link['url'] ) . '" class="cookies-banner__button">' . esc_html( $cookies_link['title'] ) . '</a>';
				}
				?>
			</section>
			<?php
				// The javascript goes in a separate file so this one is primarily for the main-footer.
				get_footer( 'scripts' );
			?>
		</div>
	</body>
</html>
