<?php
/**
 *
 * Functions for the <CLIENT-NAME> <YEAR> website theme
 *
 * @package NAMEOFTHEME
 */

/*
====================================================================================================
 Global functions for editing/adding to the core WordPress functionality, e.g.

 - Tidying up the WordPress Dashboard (CMS)
 - Protecting the WordPress system and the theme from potential security threats
 - Removing/editing unnecessary default WordPress theme functions (wp_head() etc.)
 - Registering custom post types and taxonomies
 - Registering custom widget/sidebar areas and shortcodes
 - Registering custom AJAX requests

 BE CAREFUL WITH WHAT YOU DO IN THIS AND ANY OTHER INCLUDED FILES!
====================================================================================================
*/

/*
====================================================================================================
 Table of Contents
====================================================================================================

 * Table of contents for easily navigating this file
*/

/*
 * 1. Security setup
 * 2. Back end tidy-up
 * 3. Front end tidy-up
 * 4. Register post types
 * 5. Register widget/sidebar areas
 * 6. Create custom shortcodes
 * 7. AJAX scripts
*/

/*
====================================================================================================
 1. Security setup
====================================================================================================

 * Add additional security measures to WordPress and the theme,
 * including disabling the XML-RPC API and editing the default login error message(s)
*/
require_once( 'functions/security-setup.php' );


/*
====================================================================================================
 2. Back end tidy-up
====================================================================================================

 * Remove/edit unnecessary core functionality/code that affects the backend
 * e.g. custom image sizes, wrappers on certain WYSIWYG elements
*/
require_once( 'functions/back-end.php' );
// ACF specific.
require_once( 'functions/acf.php' );


/*
====================================================================================================
 3. Front end tidy-up
====================================================================================================

 * Remove/edit unnecessary core functionality/code from the theme and its functions,
 * e.g. clean up wp_head() and anything that affects html output
*/
require_once( 'functions/front-end.php' );


/*
====================================================================================================
 4. Register post types
====================================================================================================

 * Create custom post types for specific website content,
 * and register any associated custom taxonomies
*/
/* Include custom post type(s) here.. */

/*
====================================================================================================
 5. Register widget/sidebar areas
====================================================================================================

 * Create widget areas/dynamic sidebars to hold bits and pieces of content,
 * which can then be included across multiple pages/posts
*/
/* Include widget/sidebar(s) here.. */

/*
====================================================================================================
 6. Create custom shortcodes
====================================================================================================

 * Create custom shortcodes for blocks of website content
*/
/* Include shortcode(s) here.. */

/*
====================================================================================================
 7. AJAX scripts
====================================================================================================

 * Load custom AJAX scripts to be handled by WordPress' AJAX engine
*/
// Include AJAX script(s) here..
