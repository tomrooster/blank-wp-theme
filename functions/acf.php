<?php
/**
 *
 * ACF options config for the <CLIENT-NAME> <YEAR> website theme
 *
 * @package NAMEOFTHEME
 */

namespace Rooster\NAMEOFTHEME;

define( 'ACF_PRO_LICENSE', 'b3JkZXJfaWQ9OTA1NTJ8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE2LTA5LTI4IDA4OjI2OjAw' );

/**
 * ===============================================================================
 * Only allow fields to be edited by roosteradmin
 */
if ( get_current_user_id() !== 1 ) {
	/**
	 * Change the ACF menu for non-dev sites
	 */
	function remove_menu_pages() {
		// Remove the default acf link.
		remove_menu_page( 'edit.php?post_type=acf-field-group' );
	}
	add_action( 'admin_init', __NAMESPACE__ . '\remove_menu_pages' );
}

/**
 * ===============================================================================
 * Add the sitewide options page.
 */
if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page( 'Sitewide' ); // General options.
}

/**
 * ===============================================================================
 * Add a bold only WYSIWYG option
 *
 * @param array $toolbars The ACF toolbars variable.
 */
function bold_only_toolbar( $toolbars ) {
	// Add a new toolbar called "Bold Only".
	// - this toolbar has only 1 row of buttons.
	$toolbars['Bold Only'] = array();
	$toolbars['Bold Only'][1] = array( 'styleselect', 'bold' );

	return $toolbars;
}
add_filter( 'acf/fields/wysiwyg/toolbars', __NAMESPACE__ . '\bold_only_toolbar' );

/**
 * ===============================================================================
 * ACF: Set the bold only option to limit the output to specific tags and default to and div
 * Note: the output can't be used inside p tags
 */
function bold_only_options() {
	?>
	<script type="text/javascript">
		acf.add_filter( 'wysiwyg_tinymce_settings', function( mceInit, id ) {
			// If there are the options above, note: it needs to match the options as output in the console.log.
			// console.log(mceInit.toolbar1);
			if (mceInit.toolbar1 === 'styleselect,bold') {
				mceInit.height = 100; // Set the height to 100px, the min-height that ACF sets is overriden in an admin.css file
				mceInit.forced_root_block = 'div'; // Set the default element to div
				mceInit.force_br_newlines = true; //!important
				mceInit.force_p_newlines = false; //!important
				mceInit.paste_as_text = true; //!important
				mceInit.resize = false; // Don't allow resize
				mceInit.valid_elements = 'div,b,strong,span[class]'; // Limit elements to div and strong/b incase a copy & paste introduces other elements
			}
			// return
			return mceInit;
		});
	</script>
	<?php
}
add_action( 'acf/input/admin_footer', __NAMESPACE__ . '\bold_only_options' );

