<?php
/**
 *
 * Back-end functions for the <CLIENT-NAME> <YEAR> website theme
 * This file does any changes that affect the WordPress side.
 *
 * @package NAMEOFTHEME
 */

namespace Rooster\NAMEOFTHEME;

/**
 *
 * Dashboard Setup dashboard_setup()
 * This adds or removes things from the backend
 */
function dashboard_setup() {
	/**
	 * ===============================================================================
	 * Remove WordPress' default content editor field
	 */
	function remove_content_field() {
		remove_post_type_support( 'page', 'editor' );
		remove_post_type_support( 'post', 'editor' );
		// Add your custom post types here ( if required ).
	}
	add_action( 'init', __NAMESPACE__ . '\remove_content_field', 100 );

	/**
	 * ===============================================================================
	 * Enable WordPress menus
	 */
	add_theme_support( 'menus' );

	/**
	 * ===============================================================================
	 * Set WordPress images in WYSIWYG editor's default display settings:
	 * Alignment: None
	 * Size: Full Size
	 * Link to: None
	 */
	function image_display_settings() {
		update_option( 'image_default_align', 'none' );
		update_option( 'image_default_size', 'full' );
		update_option( 'image_default_link_type', 'none' );
	}
	add_action( 'admin_init', __NAMESPACE__ . '\image_display_settings', 10 );

	/**
	 * ===============================================================================
	 * Remove WordPress' default image sizes ( except for thumbnail )
	 *
	 * @param array $sizes The array of default image sizes.
	 */
	function remove_default_image_sizes( $sizes ) {
		unset( $sizes['medium'] );
		unset( $sizes['medium_large'] );
		unset( $sizes['large'] );
		return $sizes;
	}
	add_filter( 'intermediate_image_sizes_advanced', __NAMESPACE__ . '\remove_default_image_sizes' );

	/**
	 * ===============================================================================
	 * Create custom image thumbnail sizes:
	 */
	/* Add your custom image sizes here.. */

	/**
	 * ===============================================================================
	 * Turn off image scaling
	 */
	add_filter( 'big_image_size_threshold', '__return_false' );

	/**
	 * ===============================================================================
	 * Move the Yoast SEO panel to the bottom of all page/post edit screens
	 */
	function yoast_to_bottom() {
		return 'low';
	}
	add_filter( 'wpseo_metabox_prio', __NAMESPACE__ . '\yoast_to_bottom' );

	/**
	 * ===============================================================================
	 * Change the default labels etc. of the 'Posts' post type
	 *
	 * @TODO stop using globals if possible, linting disabled for now.
	 */
	function change_posts_sidebar_labels() {
		global $menu;
		global $submenu;

		// phpcs:disable
		$menu[5][0] = 'Blog Posts'; // Change 'Posts' to 'Blog Posts'.
		$menu[5][6] = 'dashicons-welcome-write-blog'; // Change the Blog Posts dashicon.
		$submenu['edit.php'][5][0] = 'All Blog Posts';
		$submenu['edit.php'][10][0] = 'Add New Blog Post';
		$submenu['edit.php'][15][0] = 'Blog Categories'; // Change the 'Categories' text.
		// phpcs:enable
	}
	add_action( 'admin_menu', __NAMESPACE__ . '\change_posts_sidebar_labels' );

	/**
	 * ===============================================================================
	 * Remove the 'Comments' links from the admin sidebar
	 */
	function remove_comments_sidebar_link() {
		remove_menu_page( 'edit-comments.php' );
	}
	add_action( 'admin_menu', __NAMESPACE__ . '\remove_comments_sidebar_link' );

	/**
	 * Remove the 'Comments' links from the topbar
	 *
	 * @param object $wp_admin_bar The object created by WP_Admin_Bar.
	 */
	function remove_comments_topbar_link( $wp_admin_bar ) {
		$wp_admin_bar->remove_menu( 'comments' );
	}
	add_action( 'admin_bar_menu', __NAMESPACE__ . '\remove_comments_topbar_link', 999 );

	/**
	 * ===============================================================================
	 * Remove the 'Customizer' links from the admin sidebar
	 */
	function remove_customizer_sidebar_link() {
		global $submenu;
		unset( $submenu['themes.php'][6] );
	}
	add_action( 'admin_menu', __NAMESPACE__ . '\remove_customizer_sidebar_link' );

	/**
	 * Remove the 'Customizer' links from the topbar
	 *
	 * @param object $wp_admin_bar The object created by WP_Admin_Bar.
	 */
	function remove_customizer_topbar_link( $wp_admin_bar ) {
		$wp_admin_bar->remove_menu( 'customize' );
	}
	add_action( 'admin_bar_menu', __NAMESPACE__ . '\remove_customizer_topbar_link', 999 );

	/**
	 * ===============================================================================
	 * Add the styleguide into the sidebar
	 *
	 * @TODO remove on go live if not in git
	 */
	function register_styleguide_link() {
		add_menu_page(
			'Styleguide',
			'Styleguide',
			'manage_options',
			'../wp-content/themes/NAMEOFTHEME/styleguide/',
			'',
			'dashicons-admin-appearance',
			99
		);
	}
	add_action( 'admin_menu', __NAMESPACE__ . '\register_styleguide_link' );

	/**
	 * ===============================================================================
	 * Reorder the links in the admin sidebar
	 * Sourced from: http://wordpress.stackexchange.com/a/141724
	 *
	 * @param bool $menu_ord Sets custom ordering to true.
	 */
	function reorder_sidebar_links( $menu_ord ) {
		if ( ! $menu_ord ) {
			return true;
		}

		return array(
			'index.php', // Dashboard.
			'separator1', // First separator.
			'edit.php?post_type=page', // Pages.
			// Custom post types here...
			'edit.php', // Posts.
			'upload.php', // Media.
			'wpcf7', // Contact form 7.
			'flamingo', // Flamingo.
			'wpseo_dashboard', // Yoast.
			'separator2', // Second separator.
			'themes.php', // Appearance.
			'plugins.php', // Plugins.
			'users.php', // Users.
			'tools.php', // Tools.
			// ACF options pages.
			'options-general.php', // Settings.
			'edit.php?post_type=acf-field-group', // ACF.
			'Wordfence', // Wordfence.
			'separator-last', // Last separator.
		);
	}
	add_filter( 'custom_menu_order', __NAMESPACE__ . '\reorder_sidebar_links' );
	add_filter( 'menu_order', __NAMESPACE__ . '\reorder_sidebar_links' );

	/**
	 * ===============================================================================
	 * Disallow theme/plugin file editing from within WordPress
	 */
	define( 'DISALLOW_FILE_EDIT', true );

	/**
	 * ===============================================================================
	 * Style the TinyMCE editor to look more like the theme/website
	 */
	add_editor_style( 'dist/css/editor-style.css' );

	/**
	 * ===============================================================================
	 * Remove the admin bar for all users
	 */
	show_admin_bar( false );
}
add_action( 'after_setup_theme', __NAMESPACE__ . '\dashboard_setup' );

/**
 * ===============================================================================
 * Deregister the 'tags' default taxonomy
 */
function deregister_tags_taxonomy() {
	register_taxonomy( 'post_tag', array() );
}
add_action( 'init', __NAMESPACE__ . '\deregister_tags_taxonomy' );

/**
 * ===============================================================================
 * Add custom styles to TinyMCE 'formats' button
 *
 * @param array $init An array with TinyMCE config.
 */
function tinymce_add_custom_formats( $init ) {
	$styles = array(
		array(
			'title' => 'Paragraph',
			'block' => 'p',
		),
		array(
			'title'   => 'Span',
			'block'   => 'span',
		),
		array(
			'title'   => 'Div',
			'block'   => 'div',
			'wrapper' => true,
		),
		array(
			'title'   => 'Clear',
			'block'   => 'div',
			'classes' => 'clearfix',
		),
		array(
			'title'   => 'Display: None',
			'block'   => 'div',
			'classes' => 'desktop-hide',
		),
		array(
			'title'    => 'Button',
			'selector' => 'a',
			'classes'  => 'button',
		),
	);

	$init['style_formats'] = json_encode( $styles );
	return $init;
}
add_filter( 'tiny_mce_before_init', __NAMESPACE__ . '\tinymce_add_custom_formats' );

/**
 * ===============================================================================
 * Return the google API key, also used in the loading of the google JS
 */
function google_api_key() {
	return 'TODO_ENTER_API_KEY';
}

/**
 * ===============================================================================
 * Filter ACF's Google Map field to apply a valid API key
 */
function acf_init() {
	$the_key = google_api_key();
	acf_update_setting( 'google_api_key', $the_key );
}
add_action( 'acf/init', __NAMESPACE__ . '\acf_init' );

/**
 * ===============================================================================
 * Remove <p> tags that wrap shortcodes in widgets
 *
 * @param string $content The content of the widget.
 */
function remove_p_tags_from_shortcodes( $content ) {
	$content = preg_replace( '/( <p> )\s*( <nav )/', '<nav', $content );
	$content = preg_replace( '/( <\/nav> )\s*( <\/p> )/', '</nav>', $content );
	return $content;
}
add_filter( 'widget_text', __NAMESPACE__ . '\remove_p_tags_from_shortcodes', 40 );

/**
 * ===============================================================================
 * Allow shortcodes in Contact Form 7
 *
 * @param not_sure_what_type $form The form in Contact Form 7.
 */
function cf7_allow_shortcodes( $form ) {
	$form = do_shortcode( $form );
	return $form;
}
add_filter( 'wpcf7_form_elements', __NAMESPACE__ . '\cf7_allow_shortcodes' );

/**
 * ===============================================================================
 * Add admin css
 */
function admin_style() {
	wp_enqueue_style( 'admin-styles', get_template_directory_uri() . '/dist/css/admin.css', array(), '1.0.0' );
}
add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\admin_style' );
