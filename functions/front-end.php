<?php
/**
 *
 * Front end functions for the <CLIENT-NAME> <YEAR> website theme
 * This file does any changes to the front end output of the site
 *
 * @package NAMEOFTHEME
 */

namespace Rooster\NAMEOFTHEME;

/**
 * ===============================================================================
 * Tidy up the wp_head() function
 */
function tidy_up_wp_head() {
	// Remove RSS feed links ( posts/comments/categories ).
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'feed_links_extra', 3 );

	// Remove RSD EditURI link.
	remove_action( 'wp_head', 'rsd_link' );

	// Remove Windows Live Writer link.
	remove_action( 'wp_head', 'wlwmanifest_link' );

	// Remove index link.
	remove_action( 'wp_head', 'index_rel_link' );

	// Remove previous link.
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );

	// Remove start link.
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );

	// Remove 'prev'/'next' post links.
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

	// Remove canonical link.
	remove_action( 'wp_head', 'rel_canonical', 10, 0 );

	// Remove shortlink link.
	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );

	// Remove WordPress generator/version link.
	remove_action( 'wp_head', 'wp_generator' );

	// Remove inline styles for recent post comments.
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );

	// Remove emoji styles/scripts.
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );

	/**
	 * Remove WP version from query strings
	 *
	 * @param string $src The source URL of the enqueued style.
	 */
	function remove_wp_ver_css_js( $src ) {
		if ( strpos( $src, 'ver=' ) ) {
			$src = remove_query_arg( 'ver', $src );
		}
		return $src;
	}
	add_filter( 'style_loader_src', __NAMESPACE__ . '\remove_wp_ver_css_js', 9999 );
	add_filter( 'script_loader_src', __NAMESPACE__ . '\remove_wp_ver_css_js', 9999 );

	/**
	 * Remove WordPress version from RSS feed
	 */
	function remove_wp_ver_rss() {
		return '';
	}
	add_filter( 'the_generator', __NAMESPACE__ . '\remove_wp_ver_rss' );

	/**
	 * Switch default core markup for search form, comment form, and comments to output valid HTML5.
	 */
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		)
	);

	/**
	 * Remove Gutenberg styles.
	 */
	function remove_gutenberg() {
		wp_dequeue_style( 'wp-block-library' );
		wp_dequeue_style( 'wp-block-library-theme' );
		wp_dequeue_style( 'wc-block-style' );
		wp_dequeue_style( 'global-styles' );
	}
	add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\remove_gutenberg' );
	remove_action( 'wp_body_open', 'wp_global_styles_render_svg_filters' ); // [TODO] Check if this works.

	/**
	 * Remove Other scripts.
	 */
	function remove_other_scripts() {
		wp_deregister_script( 'regenerator-runtime' );
		wp_deregister_script( 'wp-polyfill' );
	}
	add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\remove_other_scripts' );

	/**
	 * Load local jQuery, or 3rd-party.js.
	 *
	 * [TODO] Remove if jquery not needed
	 */
	function unload_jquery() {
		// only use this method if we're not in wp-admin.
		if ( ! is_admin() ) {
			// deregister the original version of jQuery.
			wp_deregister_script( 'jquery' );

			// register jQuery as CF7 doesn't work without a script registered with the name jquery ( if a conbmined 3rd-party scripts file is being used it can be loaded here ).
			wp_register_script( 'jquery', esc_url( get_template_directory_uri() ) . '/dist/js/jquery.min.js', array(), '3.3.1', true );

			// add it back into the queue.
			wp_enqueue_script( 'jquery' );
		}
	}
	add_action( 'template_redirect', __NAMESPACE__ . '\unload_jquery' );
}
add_action( 'init', __NAMESPACE__ . '\tidy_up_wp_head' );

/**
 * ===============================================================================
 * Add menu levels to all menus
 *
 * @param array $menu The menu items, sorted by each menu item's menu order.
 */
function menu_class( $menu ) {
	$level = 0;
	$stack = array( '0' );
	foreach ( $menu as $key => $item ) {
		while ( $item->menu_item_parent != array_pop( $stack ) ) {
			$level--;
		}
		$level++;
		$stack[] = $item->menu_item_parent;
		$stack[] = $item->ID;
		$menu[ $key ]->classes[] = 'level-' . ( $level - 1 );
	}
	return $menu;
}
add_filter( 'wp_nav_menu_objects', __NAMESPACE__ . '\menu_class' );

/**
 * ===============================================================================
 * Deregister wp-embed.js
 */
function deregister_wp_embed() {
	// Only use this method if we're not in wp-admin.
	if ( ! is_admin() ) {
		wp_deregister_script( 'wp-embed' );
	}
}
add_action( 'wp_footer', __NAMESPACE__ . '\deregister_wp_embed' );

/**
 * ===============================================================================
 * Add iFrame to allowed wp_kses_post tags
 *
 * @param array  $tags Allowed tags, attributes, and/or entities.
 * @param string $context Context to judge allowed tags by. Allowed values are 'post'.
 *
 * @return array
 */
function custom_wpkses_post_tags( $tags, $context ) {
	if ( 'post' === $context ) {
		$tags['iframe'] = array(
			'src'             => true,
			'height'          => true,
			'width'           => true,
			'frameborder'     => true,
			'allowfullscreen' => true,
			'loading'         => true,
		);
	}
	return $tags;
}
add_filter( 'wp_kses_allowed_html', __NAMESPACE__ . '\custom_wpkses_post_tags', 10, 2 );

/**
 * ===============================================================================
 * Add a wrapper <div> around all <iframe> tags ( for presentational & responsive purposes )
 *
 * @param string $content The content of the current post/acf field.
 */
function add_iframe_wrapper( $content ) {
	if ( ! is_admin() && preg_match( '~<iframe.*?</iframe>~is', $content ) ) {
		$content = preg_replace_callback( '~<iframe.*?</iframe>~is', __NAMESPACE__ . '\add_frame_wrapper', $content );
		// Also add lazy loading to all iframes.
		$content = preg_replace( '/(<iframe\b[^><]*)>/i', '$1 loading="lazy">', $content );
	}
	return $content;
}
add_filter( 'the_content', __NAMESPACE__ . '\add_iframe_wrapper', 30 );
add_filter( 'acf_the_content', __NAMESPACE__ . '\add_iframe_wrapper', 30 );
add_filter( 'widget_text', __NAMESPACE__ . '\add_iframe_wrapper', 30 );

/**
 * Anonymous function to add the wrapper called in preg_replace_callback
 * See http://php.net/manual/en/function.preg-replace-callback.php
 *
 * @param string $matches The content from the preg_replace_callback.
 */
function add_frame_wrapper( $matches ) {
	return '<div class="iframe-wrap clearfix">' . "\n" . $matches[0] . "\n" . '</div>';
}

/**
 * ===============================================================================
 * Remove <p> tags that wrap images in WYSIWYG content
 *
 * @param string $content The content of the current post/acf field.
 */
function remove_p_tags_from_images( $content ) {
	return preg_replace( '/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content );
}
add_filter( 'the_content', __NAMESPACE__ . '\remove_p_tags_from_images', 10 );
add_filter( 'acf_the_content', __NAMESPACE__ . '\remove_p_tags_from_images', 11 );
add_filter( 'widget_text', __NAMESPACE__ . '\remove_p_tags_from_images', 10 );

/**
 * ===============================================================================
 * Add a wrapper <div> around all <table> tags ( for presentational & responsive purposes )
 * Sourced and edited from https://stackoverflow.com/a/40328327.
 *
 * @param string $content The content of the current post/acf field.
 */
function add_table_wrapper( $content ) {
	if ( ! is_admin() && preg_match( '~<table.*?</table>~is', $content ) ) {
		$content = preg_replace_callback( '~<table.*?</table>~is', __NAMESPACE__ . '\add_wrapper', $content );
	}
	// Then return the result.
	return $content;
}
add_filter( 'the_content', __NAMESPACE__ . '\add_table_wrapper', 30 );
add_filter( 'acf_the_content', __NAMESPACE__ . '\add_table_wrapper', 30 );
add_filter( 'widget_text', __NAMESPACE__ . '\add_table_wrapper', 30 );

/**
 * Anonymous function to add the wrapper called in preg_replace_callback
 * See http://php.net/manual/en/function.preg-replace-callback.php
 *
 * @param string $matches The content from the preg_replace_callback.
 */
function add_wrapper( $matches ) {
	// Remove inline style tags as well.
	// https://stackoverflow.com/a/5518159.
	$content = preg_replace( '/ style="(.+?)"(.+?)/i', '$2', $matches[0] );
	return '<div class="table-wrapper">' . "\n" . $content . "\n" . '</div>';
}
