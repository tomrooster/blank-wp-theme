<?php
/**
 *
 * Security fuctions for the <CLIENT-NAME> <YEAR> website theme
 *
 * @package NAMEOFTHEME
 */

namespace Rooster\NAMEOFTHEME;

/**
 * ===========================================================================================================================================
 * Remove WordPress' X-Pingback HTTP header
 * Don't forget to disable access to the xmlrpc.php file in .htaccess!
 *
 * @param string $headers  Associative array of headers to be sent.
 */
function remove_x_pingback_header( $headers ) {
	unset( $headers['X-Pingback'] );
	return $headers;
}
add_filter( 'wp_headers', __NAMESPACE__ . '\remove_x_pingback_header' );

/**
 * ===========================================================================================================================================
 * Edit the default error message on invalid login
 */
function failed_login() {
	return 'The login information you have entered is incorrect.';
}
add_filter( 'login_errors', __NAMESPACE__ . '\failed_login' );


/**
 * ===========================================================================================================================================
 * Edit the default message on the 'Forgot Password' page
 */
function forgotpassword_message() {
	if ( isset( $_REQUEST['action'] ) ) {
		$action = sanitize_text_field( wp_unslash( $_REQUEST['action'] ) );
		if ( $action == 'lostpassword' ) {
			$message = '<p class="message">If the username entered is correct, you will receive further instructions via email.</p>';
			return $message;
		}
	}
}
add_filter( 'login_message', __NAMESPACE__ . '\forgotpassword_message' );
