'use strict';
 
/*
	Variables
	- requires for Gulp
	- postCSS settings
	- paths
	- styleguide settings
 */

var gulp = require('gulp');
var sass = require('gulp-dart-sass');
var terser = require('gulp-terser');
var pump = require('pump');
var imagemin = require('gulp-imagemin');
var flatten = require('gulp-flatten');
var filter = require('gulp-filter');
var postcss = require('gulp-postcss');
var concat = require('gulp-concat');
var fs = require('fs');
var sasslint = require('gulp-sass-lint');
var eslint = require('gulp-eslint');
var kss = require('kss');
var phpcs = require('gulp-phpcs');
var glob = require('glob');
var path = require('path');
var gulpopen = require('gulp-open');
var exit = require('gulp-exit');

var postcssProcessorsDev = [
	require('autoprefixer')({grid: 'autoplace'})
];
var postcssProcessorsProd = [
	require('autoprefixer')({grid: 'autoplace'}),
	require('cssnano')()
];

// Paths are mostly self explanatory
// srcSassRoot: Is only used to create the import maps
// srcSass: Is for watching/building sass, it ignores the import maps to avoid double compiling, and there's never anything in there that need building
// phpFiles: Is just for linting, it avoids anything installed by node/bower/composer
var paths = {
	srcFiles: './src/',
	srcJs: './src/js/**/*.js',
	srcSassRoot: './src/sass/*/',
	srcSass: ['./src/sass/**/*.scss', '!./src/sass/import-maps/*.scss'],
	srcSassAll: './src/sass/**/*.scss',
	srcCSSLibs: './src/sass/libraries/css/',
	srcImages: './src/images/**/*',
	phpFiles: ['**/*.php', '!bower_components/**/*.*', '!node_modules/**/*.*', '!reports/**/*.*', '!wpcs/**/*.*', '!acf-json/**/*.*', '!vendor/**/*.*'],
	distImages: './dist/images/',
	distCss: './dist/css/',
	distJs: './dist/js/',
	webrootDist: '/wp-content/themes/NAMEOFTHEME/dist/'
};

var styleGuide = {
	source: [
		'src/sass/'
	],
	destination: 'styleguide/',
	css: [
		"../dist/css/styles.css", 
		"../dist/css/other-styles.css", 
		"../dist/css/style-guide.css"
	],
	js: [
		"../dist/js/jquery.min.js", 
		"../dist/js/scripts.js"
	],
	homepage: './README.md',
	title: '<CLIENT-NAME>'
};

/**
	Move Files
	- This copies files from npm/bower
**/
gulp.task('movefiles', function (cb) {
	// Add any files that need moving in here
	// Callback
	cb();
});

/**
	CSS / Sass tasks
	- sass:dev - the watch task (readable css)
	- sass:prod - the build task (compressed css)
	- sass:lint - linting the sass
**/

gulp.task('sass:dev', function (cb) {
	gulp.src(paths.srcSass)
		.pipe(sass({
			outputStyle: 'expanded',
			errLogToConsole: true
		}))
		.pipe(postcss(postcssProcessorsDev))
		.pipe(gulp.dest(paths.distCss));
	// Callback
	cb();
});

gulp.task('sass:prod', function (cb) {
	gulp.src(paths.srcSass)
		.pipe(sass({
			outputStyle: 'compressed',
			errLogToConsole: true
		}))
		.pipe(postcss(postcssProcessorsProd))
		.pipe(gulp.dest(paths.distCss));
	// Callback
	cb();
});

gulp.task('sass:lint', function () {
	return gulp.src(paths.srcSass)
		.pipe(sasslint({
			configFile: 'config/sass-lint.yml'
		}))
		.pipe(sasslint.format())
		.pipe(sasslint.failOnError())
});

// Create import maps rather than relying on globbing.
// This goes through all the sass dirs and creates a list of files to import for each, the same outcome as globbing but easier to use without gulp
// Based on https://nateeagle.com/2014/05/22/sass-directory-imports-with-gulp/
gulp.task('sass:import_maps', function (cb) {
	var exclude = ['import-maps','sass'];
	// Loop through all the directories in /src/sass/
	glob(paths.srcSassRoot, function (error, files) {
		files.forEach(function (allFile) {
			// Check the name of the current directory, if it's not one of the excluded...
			var current_dir = path.basename(allFile);
			var directory = path.dirname(allFile) + '/' + current_dir;
			if ( !exclude.includes(current_dir) ) {
				// ... search through all the directories in the current directory
				glob(directory + '/**/', function (error, files) {
					// Create the file and add a note saying it's auto generated
					var import_map_name = './src/sass/import-maps/_' + current_dir + '.scss';
					fs.writeFileSync(import_map_name, '// This is a dynamically generated file\n\n');
					// Cycle through all the files in the current directory & its sub directories
					files.forEach(function (allSubFile) {
						var current_dir = path.basename(allSubFile);
						var sub_directory = path.dirname(allSubFile) + '/' + current_dir;
						var partials = fs.readdirSync(sub_directory).filter(function (file) {
							return (
								// Only include _*.scss files
								path.basename(file).substring(0, 1) === '_' &&
								path.extname(file) === '.scss'
							);
						});
						// Append import statements for each partial
						partials.forEach(function (partial) {
							// Note: the imports are relative to /src/sass/ which is removed from the string
							fs.appendFileSync(import_map_name, "@import '" + sub_directory.replace('./src/sass/', '') + "/" + partial + "';\n");
						});
					});
				});
			}
		});
	});
	// Callback
	cb();
});

/**
	Javascript tasks
	- js:dev - the watch task (readable js)
	- js:prod - the build task (compressed js)
	- js:lint - linting the js using eslint
	- concat:js - conact task for the 3rd party files 
**/

gulp.task('js:dev', function (cb) {
	gulp.src(paths.srcJs)
		.pipe(flatten())
		.pipe(gulp.dest(paths.distJs));
	// Callback
	cb();
});

gulp.task('js:prod', function (cb) {
	return gulp.src(paths.srcJs)
		.pipe(terser())
		.pipe(gulp.dest(paths.distJs));
	// Callback
	cb();
});

gulp.task('js:lint', function (cb) {
	return gulp.src(paths.srcJs)
		.pipe(eslint({
			// Load a specific ESLint config
			configFile: 'config/eslint-config.json'
		}))
		.pipe(eslint.format())
		.pipe(eslint.failOnError())
	// Callback
	cb();
});

gulp.task('concat:js', function (cb) {
//	gulp.src([
//		paths.distJs+'jquery.min.js',
//		paths.distJs+'owl.carousel.min.js',
//	])
//		.pipe(concat('3rd-party.js'))
//		.pipe(gulp.dest(paths.distJs));
	// Callback
	cb();
});

/**
	PHP tasks
	- php:lint - linting the php, runs PHPCS using the WordPress standard with the following overrides:
		- WordPress.PHP.YodaConditions - because pointless Yoda conditionals mostly are
		- WordPress.WhiteSpace.DisallowInlineTabs - tabs are better for indenting arrays
		- WordPress.WP.EnqueuedResources - enqueing scripts is less important than performance on custom themes
**/

gulp.task('php:lint', function () {
	return gulp.src(paths.phpFiles)
	// Validate files using PHP Code Sniffer
	.pipe(phpcs({
		bin: 'vendor/bin/phpcs',
		standard: 'WordPress',
		exclude: ['WordPress.PHP.YodaConditions', 'WordPress.WhiteSpace.DisallowInlineTabs', 'WordPress.WP.EnqueuedResources'],
		warningSeverity: 0
	}))
	// Log all problems that was found
	.pipe(phpcs.reporter('log'));
});


/**
	Image tasks
	- imagemin - moves & minifies images from src to dist
**/

gulp.task('imagemin', function (cb) {
	gulp.src(paths.srcImages)
		.pipe(imagemin())
		.pipe(gulp.dest(paths.distImages));
	// Callback
	cb();
});

/**
	Styleguide tasks
	Based on https://blog.greggant.com/posts/2018/01/16/integrating-node-kss-with-gulp.html
	- kss - builds the styleguide
**/
gulp.task('kss', function(cb) {
	kss(styleGuide, cb);
	// Callback
	cb();
});

/**
	Service workers
	This defines which files to watch and what functions to run when they change
	- init - this creates an sw.json file to keep track of the version of the cache, that needs to change on each build
	- build - sets the name of the new cache based on version number, sets the files to be cached, then creates the service worker which:
		- deletes all old caches
		- creates the new one
		- sets it up to load things from the cache
**/

gulp.task('serviceworker:init', function(cb){
	// Create a .json file to track the version numer
	fs.writeFile('config/sw.json', '{ "version": "0" }', cb);
	// Callback
	cb();
});

gulp.task('serviceworker:build', function(cb){
	// Read the current version from the .json file
	var swCacheVersion = JSON.parse(fs.readFileSync('./config/sw.json'));
	// Set the current and new as variables
	var swCacheVersionNum = parseInt(swCacheVersion.version);
	var swCacheVersionNew = (swCacheVersionNum+1);
	// Set the old & new cache names
	var swCacheNameNew = 'NAMEOFTHEME-' + swCacheVersionNew;
	// List the files that will be in the cache, this should be all the css files 
	var swFiles = [
		"'"+paths.webrootDist+"css/styles.css'",
		"'"+paths.webrootDist+"css/other-styles.css'",
		"'"+paths.webrootDist+"js/jquery.min.js'",
		"'"+paths.webrootDist+"js/scripts.js'"
	];
	// Create the sw.js file
	// Set up the variables
	var swContents = "var CACHE_NAME = '"+swCacheNameNew+"';\n";    
	var swContents = swContents + "var urlsToCache = ["+swFiles+"]\n\n";
	// Add the install event to add files to the cache & delete the previous
	var swContents = swContents + "self.addEventListener('install', function(event) {\n";
	var swContents = swContents + "  // Perform install steps\n";
	// Delete old caches
	var swContents = swContents + "  event.waitUntil(\n";
	var swContents = swContents + "    caches.keys().then(function(cacheNames) {\n";
	var swContents = swContents + "      return Promise.all(\n";
	var swContents = swContents + "        cacheNames.map(function(cacheName) {\n";
	var swContents = swContents + "          if (cacheName !== CACHE_NAME) {\n";
	var swContents = swContents + "            return caches.delete(cacheName);\n";
	var swContents = swContents + "          }\n";
	var swContents = swContents + "        })\n";
	var swContents = swContents + "      );\n";
	var swContents = swContents + "    })\n";
	var swContents = swContents + "  );\n";
	// Add new cache
	var swContents = swContents + "  event.waitUntil(\n";
	var swContents = swContents + "    caches.open(CACHE_NAME)\n";
	var swContents = swContents + "      .then(function(cache) {\n";
	var swContents = swContents + "        return cache.addAll(urlsToCache);\n";
	var swContents = swContents + "      })\n";
	var swContents = swContents + "  );\n";
	var swContents = swContents + "});\n";
	// Add the fetch event to load from the cache
	var swContents = swContents + "self.addEventListener('fetch', function(event) {\n";
	// Safai has issues with service workers and mp4s, https://adactio.com/journal/14452
	var swContents = swContents + "  if ( event.request.url.match(/\.(mp4)/) ) {\n";
	var swContents = swContents + "    return;\n";
	var swContents = swContents + "  }\n";
	// Resume the fetch
	var swContents = swContents + "  event.respondWith(\n";
	var swContents = swContents + "    caches.match(event.request)\n";
	var swContents = swContents + "      .then(function(response) {\n";
	var swContents = swContents + "        // Cache hit - return response\n";
	var swContents = swContents + "        if (response) {\n";
	var swContents = swContents + "          return response;\n";
	var swContents = swContents + "        }\n";
	var swContents = swContents + "        return fetch(event.request);\n";
	var swContents = swContents + "      }\n";
	var swContents = swContents + "    )\n";
	var swContents = swContents + "  );\n";
	var swContents = swContents + "});";
	// Write the service worker file, this assumes the theme is in /wp-content/themes/NAMEOFTHEME/
	fs.writeFile('../../../sw.js', swContents, cb);
	// Update the version number in the sw.json file	
	fs.writeFile('sw.json', '{ "version": "'+swCacheVersionNew+'" }', cb);
	// Callback
	cb();
});


/**
	Watch
	- This defines which files to watch and what functions to run when they change
**/

function watchFiles() {
	gulp.watch(paths.srcSass, gulp.series('sass:import_maps', 'sass:dev', 'sass:lint', 'kss')); // Create the css, lint the sass, update the styleguide
	gulp.watch(paths.srcJs, gulp.series('js:dev', 'js:lint', 'concat:js')); // Move and lint the js
	gulp.watch(paths.srcImages, gulp.series('imagemin')); // Compress all images
	gulp.watch(paths.phpFiles, gulp.series('php:lint')); // Lint php files
}

gulp.task("watch", gulp.parallel(watchFiles));


/**
	Global tasks
	- Default / watch
	- Build
	- Init
**/

gulp.task('default', gulp.parallel('watch'));

gulp.task('build', gulp.series(
	'sass:import_maps',
	'sass:prod',
	'sass:lint',
	'js:prod',
	'js:lint',
	gulp.parallel('imagemin', 'kss'),
//	'serviceworker:build',
));

gulp.task('init', gulp.series(
	'movefiles',
	'build'
));