<?php
/**
 *
 * Template for displaying the <head> of the <CLIENT-NAME> <YEAR> website theme
 * This does all the meta information, loads the stylesheets & favicons, and anything else that's appropriate
 *
 * @package NAMEOFTHEME
 */

?>
<?php // Page title and meta data. ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<title><?php wp_title( '' ); ?></title>
<?php
/**
 * Preconnecting for DNS / preloading
 * Preload main css and any font files
 * Preload for the javascript files that'll definitely be loaded
 * If any are uncertain add them as link rel="prefetch"
 * The preconnect/dns-prefetch for any 3rd party domains
 * see: https://medium.com/reloading/preload-prefetch-and-priorities-in-chrome-776165961bbf
 */

// DNS prefetching / preconnecting for External domains.
// If there are any domains being used on a single page/template (ie Google maps on contact) they should be manually loaded.
// Note: Preconnect and dns-prefect should be separate.
?>
<!--link rel="preconnect"   href="https://example.com" crossorigin-->
<!--link rel="dns-prefetch" href="https://example.com"-->
<?php
// Any `async` javascript should come after this.
?>
<?php
// The main stylesheet.
$main_css_time = gmdate( 'YmdHi', filemtime( get_stylesheet_directory() . '/dist/css/styles.css' ) );
$main_js_time = gmdate( 'YmdHi', filemtime( get_stylesheet_directory() . '/dist/js/scripts.js' ) );
?>
<link rel="stylesheet" href="<?php print esc_url( get_template_directory_uri() . '/dist/css/styles.css?c=' . $main_css_time ); ?>" type="text/css">
<?php
// Preloads for Secondary css, font, and JS files that aren't above this.
?>
<link rel="preload" href="<?php print esc_url( get_template_directory_uri() . '/dist/css/other-styles.css' ); ?>" as="style">
<!--link rel="preload" href="<?php print esc_url( get_template_directory_uri() . '/fonts/font-name.woff2' ); ?>" as="font" type="font/woff2" crossorigin-->
<link rel="prefetch" href="<?php print esc_url( get_template_directory_uri() . '/dist/js/scripts.js?v=' . $main_js_time ); ?>" as="script">
<?php
// Any `defer` javascript should come after this.
?>
<?php
// The rest of the css loads asynchronously to avoid render blocking.
// https://timkadlec.com/remembers/2020-02-13-when-css-blocks/.
$other_css_time = gmdate( 'YmdHi', filemtime( get_stylesheet_directory() . '/dist/css/other-styles.css' ) );
?>
<link rel="stylesheet" href="<?php print esc_url( get_template_directory_uri() . '/dist/css/other-styles.css?v=' . $other_css_time ); ?>" media="print" onload="this.media='all'">
<noscript><link rel="stylesheet" href="<?php print esc_url( get_template_directory_uri() . '/dist/css/other-styles.css' ); ?>" media="all"></noscript>
<?php
// All other meta/head bits should come after this.
?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php // Favicons. ?>
<?php
	/*
	 * TODO
	 * - replace with generated favicons from https://realfavicongenerator.net
	 * - make sure there's a 512x512 and 192x192
	 * - take out the manifest realfavicongenerator supplies and use the one already here
	 * - edit the manifest as needed
	 * - see https://css-tricks.com/favicons-how-to-make-sure-browsers-only-download-the-svg-version/
	 */
?>
<link rel="manifest" href="<?php print esc_url( get_template_directory_uri() ); ?>/manifest.json">
<link rel="icon" type="image/png" href="<?php print esc_url( get_template_directory() ); ?>/favicon.png" sizes="48x48">
<?php wp_head(); ?>
