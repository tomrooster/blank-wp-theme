<?php
/**
 *
 * Template for displaying the header html of the <CLIENT-NAME> <YEAR> website theme
 * Outputs the html and loads the header-head.php
 *
 * @package NAMEOFTHEME
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<?php
			// The <head> content is in a separate file so this file is primarily for the main-header.
			get_header( 'head' );
		?>
	</head>
	<body <?php body_class(); ?>>
		<div class="page-wrapper">
			<span class="skip-link"><a href="#main-content">Skip to main content</a></span>
			<header class="main-header">
				<div class="main-header__logo">
					<a href="<?php print esc_url( home_url( '/' ) ); ?>">
						<img class="logo" alt="<CLIENT-NAME>" src="<?php print esc_url( get_template_directory_uri() ); ?>/dist/images/logo.svg">
					</a>
				</div>
				<nav class="main-header__nav">
					<?php
						wp_nav_menu(
							array(
								'menu' => 'main-menu',
								'menu_class' => 'main-menu',
							)
						);
						?>
				</nav>
			</header><?php // END header. ?>
		
			<main id="main-content" class="content-wrapper">
