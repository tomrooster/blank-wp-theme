<?php
/**
 *
 * Generic page template of the <CLIENT-NAME> <YEAR> website theme
 * Outputs all of the main page content (text/images/menus etc.)
 * This will be used when no other specific template(s) can be found (e.g. page-{slug}.php)
 * Finishes at the end of 'the loop' - the query that gets the current page content
 *
 * @package NAMEOFTHEME
 */

get_header();
?>

	<article class="page">
		<section class="article-main">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					?>
					<header class="article-main__header">
						<h1><?php the_title(); ?></h1>
					</header>
					<?php the_content(); ?>
					<?php
				endwhile;
			endif;
			?>
		</section>
	</article>
		
<?php
get_footer();
