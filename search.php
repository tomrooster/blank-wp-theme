<?php
/**
 *
 * Page template for the Search results page of the <CLIENT-NAME> <YEAR> website theme
 * Outputs a list of results based on the user's search query
 * Finishes at the end of 'the loop' - the query that outputs the search results
 *
 * @package NAMEOFTHEME
 */

get_header();
?>

	<article class="page">
		<section class="article-main">
			<?php
			if ( have_posts() ) {
				?>
				<header class="article-main__header">
					<h1>Search results for "<?php print esc_html( get_search_query() ); ?>"</h1>
				</header>
				<?php
				while ( have_posts() ) :
					the_post();
					print '<h2><a href="' . esc_url( get_the_permalink() ) . '">' . esc_html( get_the_title() ) . '</a></h2>';
				endwhile;
			} else {
				?>
				<header class="article-main__header">
					<h1>Sorry!</h1>
				</header>
				<p>No results were found for "<?php print esc_html( get_search_query() ); ?>". Please try another search term(s).</p>
				<?php
			}
			wp_reset_query();
			?>
		</section>
	</article>

<?php
get_footer();
