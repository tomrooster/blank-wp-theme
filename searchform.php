<?php
/**
 *
 * Search form is just the form tag for search.
 * Based on the searchform.php from TwentySeventeen
 *
 * @package NAMEOFTHEME
 */

	$unique_id = esc_attr( uniqid( 'search-form-' ) );
?>

<form role="search" method="get" class="search-form" action="<?php print esc_url( home_url( '/' ) ); ?>">
	<div class="search-form__input">
		<label class="search-form__label" for="<?php print esc_attr( $unique_id ); ?>">Search</label>
		<input type="search" id="<?php print esc_attr( $unique_id ); ?>" class="search-form__field" placeholder="Search" value="<?php print esc_attr( get_search_query() ); ?>" name="s">
	</div>
	<button type="submit" class="search-form__submit">Search</button>
</form>
