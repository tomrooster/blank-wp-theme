<?php
/**
 *
 * Sidebar template for the <CLIENT-NAME> <YEAR> website theme
 * Outputs the recent comments, post tags etc, all called from a custom widget/sidebar area
 * Finishes after the output of the last element in the widget/sidebar area
 *
 * @package NAMEOFTHEME
 */

dynamic_sidebar( 'sidebar' );
