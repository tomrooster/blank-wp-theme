/*
=================================================================
 Global JavaScript (jQuery) functions for use throughout the site
=================================================================
*/

/*
=================================================================
 Variables
=================================================================
*/

var root = document.documentElement;
// TODO get correct path for theme, project-wide search and replace
var themePath = '/wp-content/themes/NAMEOFTHEME/'; // For loading any assets
var breakpoint = {};
var didScroll = false;

// Keyboard vars
var keyEnter = 13;
var keyEscape = 27;
var keyDown = 40;
var keyUp = 38;

// Animation / reduced motion checks
var media = '(prefers-reduced-motion: reduce)';
var pref = window.matchMedia(media);

// Cookies based on https://itsgus.dev/2021/vanilla-js-gdpr-cookie-banner-no-jquery
// https://codepen.io/gschiavon/pen/wvoWZwB
const cookieBanner = document.getElementById('cookies-banner');
const cookieConsent = document.getElementById('cookie-consent');
const scripts = document.getElementById('scripts');
let savedPrefs = localStorage.getItem('consents');
let savedPrefsDate = localStorage.getItem('consent-date');
// Dates
var today = new Date().toISOString().slice(0, 10);
var cookieLifespan = new Date();
cookieLifespan.setMonth(cookieLifespan.getMonth() - 1); // Set to 1 month.
cookieLifespan = cookieLifespan.toISOString().slice(0, 10);

// IE
var isIE = false;
/* eslint-disable */
if (navigator.appVersion.indexOf('MSIE 10') !== -1 || !!window.MSInputMethodContext && !!document.documentMode) {
	isIE = true;
}
/* eslint-enable */

/*
=================================================================
 Functions
=================================================================
*/

// Check for touch screen
function isTouchDevice () {
	return (('ontouchstart' in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));
}

// Note: as per - https://johnresig.com/blog/learning-from-twitter/ - not doing the change on scroll. Running a setIntervall at 60fps to do the tweaks.
function onscrolling () {
	didScroll = true;
}

/**
 * Simulate a click event.
 * https://gomakethings.com/how-to-simulate-a-click-event-with-javascript/
 * Not needed if jQuery is being used
 */
var simulateClick = function (elem) {
	// Create our event (with options)
	var clickEvent = new MouseEvent('click', {
		bubbles: true,
		cancelable: true,
		view: window,
	});
	// If cancelled, don't dispatch our event
	var canceled = !elem.dispatchEvent(clickEvent);
};

/**
 * Get parents function
 * https://gomakethings.com/how-to-get-all-parent-elements-with-vanilla-javascript/
 * Not needed if jQuery is being used
 */
var getParents = function (elem, selector) {
	// Element.matches() polyfill
	if (!Element.prototype.matches) {
		Element.prototype.matches =
			Element.prototype.matchesSelector ||
			Element.prototype.mozMatchesSelector ||
			Element.prototype.msMatchesSelector ||
			Element.prototype.oMatchesSelector ||
			Element.prototype.webkitMatchesSelector ||
			function (s) {
				var matches = (this.document || this.ownerDocument).querySelectorAll(s);
				var i = matches.length;
				while (--i >= 0 && matches.item(i) !== this) {}
				return i > -1;
			};
	}
	// Set up a parent array
	var parents = [];
	// Push each parent element to the array
	for (; elem && elem !== document; elem = elem.parentNode) {
		if (selector) {
			if (elem.matches(selector)) {
				parents.push(elem);
			}
			continue;
		}
		parents.push(elem);
	}
	// Return our parent array
	return parents;
};

// Hide an element when the user clicks outside of it
// Usage: add to the document mouseup function
// closeElementOnClickOutside( e, 'plain-class-of-target', '.class-of-container-with-dot' );
function closeElementOnClickOutside (e, targetClass, targetContainer) {
	// Get the classes on the clicked element
	var clickTargetClass = e.target.classList;
	// Get the parent's of the clicked element if they have the targetContainer class
	var parents = getParents(e.target, targetContainer);
	if (targetClass === clickTargetClass.value) {
		// If the target has been clicked: do nothing
	} else if (parents.length) {
		// Or if the parent's contain the target container: do nothing
	} else {
		// Otherwise a click is triggered on the targetClass, this assumes the targetClass is a button with on/off click functionality
		// simulateClick(document.querySelector('.'+targetClass));
	}
}
/* jQuery Version
 * Hide an element when the user clicks outside of it
 * Usage: add to the document mouseup function
 * closeElementOnClickOutside( e, '.class-of-container-with-dot', $(normal jquery selector) );
//function closeElementOnClickOutside(e, targetClass, targetContainer) {
//	if ( $(targetClass).is(e.target) ) {
//		// If the target container has been clicked
//	} else if ( !targetContainer.is(e.target) && targetContainer.has(e.target).length === 0 ) {
//		// If it hasn't a click is triggered on the targetClass, this assumes the targetClass is a button with on/off click functionality
//		$(targetClass).trigger('click');
//	}
//}
*/

// Check the breakpoint
// https://www.lullabot.com/articles/importing-css-breakpoints-into-javascript
breakpoint.refreshValue = function () {
	this.value = window.getComputedStyle(document.querySelector('body'), ':before').getPropertyValue('content').replace(/"/g, '');
};

/*
=================================================================
 Marketing scripts, loads any scripts that require cookie consent here.
=================================================================
*/

function marketingScripts () {
	console.log('cookies accepted!');
}

/*
=================================================================
 Document ready
=================================================================
*/

;(function () {
	document.addEventListener('DOMContentLoaded', function (event) {
		/*
		==================================
		 Add the pseudo-vh
		==================================
		*/
		var vh = window.innerHeight * 0.01;
		document.documentElement.style.setProperty('--vh', vh + 'px');
		/* ====== */

		/*
		==================================
		 Add the js & ie classes
		==================================
		*/
		var html = document.querySelector('html');
		html.classList.remove('no-js');
		html.classList.add('js');
		if (isIE) {
			html.classList.add('ie');
		}

		/*
		==================================
		 Cookies
		==================================
		*/
		// Check for saved preferences
		if (savedPrefs) {
			// If there are check if they're more than a month old.
			if (savedPrefsDate < cookieLifespan) {
				// Ask again if so.
				localStorage.removeItem('consents');
				cookieBanner.classList.remove('hidden');
			} else {
				// If not it's fine.
				cookieBanner.classList.add('hidden');
				marketingScripts();
			}
		} else if (!savedPrefs) {
			cookieBanner.classList.remove('hidden');
		}

		// Consent click.
		cookieConsent.addEventListener('click', function () {
			localStorage.setItem('consents', 'accepted');
			localStorage.setItem('consent-date', today);
			cookieBanner.classList.add('hidden');
			marketingScripts();
		});
		/* ====== */
	});
}());

/*
==================================
 Document keyup
==================================
*/

;(function () {
	document.addEventListener('keyup', keyupCheck);
	function keyupCheck (e) {
		var key = e.keyCode || e.which;
		// On pressing escape hide the nav or search
		if (key === keyEscape) { // [Escape]

			// Close the nav
			// if ( document.body.classList.contains('nav--open') ) {
			// 	simulateClick(document.querySelector('.hamburger'));
			// }
		}
	}
}());

/*
==================================
 Document mouseup
==================================
*/

;(function () {
	document.addEventListener('mouseup', mouseUpFunctions);
	function mouseUpFunctions (e) {
		// Close the nav
		// if ( document.body.classList.contains('nav--open') ) {
		// 	closeElementOnClickOutside( e, 'ham', '.nav' );
		// }
	}
}());

/*
==================================
 Check breakpoint on resize
==================================
*/

;(function () {
	window.addEventListener('resize', bpresize, false);
	function bpresize () {
		breakpoint.refreshValue();
		// Update the pseudo-vh
		vh = window.innerHeight * 0.01;
		document.documentElement.style.setProperty('--vh', vh + 'px');
	}
}());

/*
==================================
 Anything running on scroll
==================================
*/

;(function () {
	window.addEventListener('scroll', onscrolling, true);
	// The set interval that actually does any scrolling bits
	setInterval(function () {
		if (didScroll) {
			// Reset didScroll.
			didScroll = false;

			// Code...
		}
	}, 16); // Note: the 16ms = approx 1000ms / 60 for 60fps
}());

/*
==================================
 On touch start, set a variable mainly for surface pro.
==================================
*/
;(function () {
	window.addEventListener('touchstart', function onFirstTouch () {
		window.USER_IS_TOUCHING = true;
		window.removeEventListener('touchstart', onFirstTouch, false);
	}, false);
}());
