<?php
/**
 * Template Name: News Template
 *
 * @package NAMEOFTHEME
 */

get_header();
?>

	<section class="page">
		<header class="page__header">
			<h1>Blog posts</h1>
		</header>
		<?php
		// Blog post loop.
		$posts_per_page = 9;
		$is_paged = ( get_query_var( 'paged' ) );
		if ( ! $offset && $is_paged == 0 ) {
			$offset = 1;
		}

		$args = array(
			'post_type'			=> 'post',
			'posts_per_page'	=> $posts_per_page,
			'post_status'		=> 'publish',
			'offset'			=> $offset,
			'paged'				=> $is_paged,
		);

		$the_query  = new WP_Query( $args );
		$no_of_posts  = count( $the_query->posts );
		$total_posts = $the_query->found_posts;
		?>

		<?php if ( $the_query->have_posts() ) : ?>

			<div id="posts" class="blog-list clearfix">
				<?php
				$counter = 1;
				while ( $the_query->have_posts() ) :
					$the_query->the_post();
					?>
					<article class="post-item blog-item">
						<header>
							<?php the_title(); ?>
						</header>
					</article>
					<?php
					$counter++;
				endwhile;
				?>
				<div class="pagination pagination--js">
					<?php
					if ( $no_of_posts < $posts_per_page || $no_of_posts < $total_posts ) {
						print '<a class="button" href="#">Load More</a>';
					}
					print '<input type="hidden" class="no-of-posts" value="' . ( esc_attr( $no_of_posts + $offset ) ) . '">';
					?>
				</div>
				<div class="pagination pagination--no-js">
					<?php
						// Does the no-js pagination.
						print '<div class="nav-links">';
						$big = 999999999; // need an unlikely integer.
						print wp_kses_post(
							paginate_links(
								array(
									'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
									'format' => '?paged=%#%',
									'current' => max( 1, get_query_var( 'paged' ) ),
									'total' => $the_query->max_num_pages,
									'prev_text'		  => __( '<span class="pagination__text">Previous</span>' ),
									'next_text'		  => __( '<span class="pagination__text">Next</span>' ),
								)
							)
						);
						print '</div>';
					?>
				</div>
			</div>
		<?php else : ?>
			<p>There are currently no news posts.</p>
		<?php endif; ?>
		<?php
			// Reset postdata after loop.
			wp_reset_postdata();
		?>
	</section>

<?php
get_footer();
